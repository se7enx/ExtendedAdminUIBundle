const path = require('path');

module.exports = (eZConfig, eZConfigManager) => {
    eZConfigManager.add({
        eZConfig,
        entryName: 'ezplatform-admin-ui-alloyeditor-js',
        newItems: [
            path.resolve(__dirname, '../public/js/alloyeditor/buttons/random.js'),
            path.resolve(__dirname, '../public/js/alloyeditor/plugins/random.js'),
        ]
    });

    eZConfigManager.replace({
        eZConfig,
        entryName: 'ezplatform-admin-ui-alloyeditor-js',
        itemToReplace: path.resolve(__dirname, '../../../../vendor/ezsystems/ezplatform-admin-ui/src/bundle/Resources/public/js/scripts/fieldType/base/base-rich-text.js'),
        newItem: path.resolve(__dirname, '../public/js/base-rich-text.js'),
    });
};
